# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Rule(metaclass=PoolMeta):
    "Analytic Rule"
    __name__ = 'analytic_account.rule'

    shop = fields.Many2One('sale.shop', 'Shop',)
