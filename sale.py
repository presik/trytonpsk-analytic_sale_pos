from trytond.model import fields
from trytond.pool import Pool, PoolMeta


class SaleShop(metaclass=PoolMeta):
    "Sale Shop"
    __name__ = 'sale.shop'

    analytic_account = fields.Many2One(
        'analytic_account.account', 'Analytic Account',
        domain=[('type', '=', 'normal')])


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    def check_for_quotation(self):
        super(Sale, self).check_for_quotation()
        pool = Pool()
        AnalyticAccountEntry = pool.get('analytic.account.entry')
        if self.shop:
            for line in self.lines:
                if line.type != 'line' and line.analytic_accounts:
                    continue
                analytic = None
                if hasattr(line.product, 'get_analytic') and self.shop:
                    analytic = line.product.get_analytic(self.shop, 'income')

                if not analytic:
                    continue
                value = {
                    'root': self.shop.analytic_account.root,
                    'account': analytic,
                }
                new_entries = AnalyticAccountEntry.create([value])
                line.analytic_accounts = new_entries
                line.save()

    @classmethod
    def process_sale_pos(cls, sale):
        pool = Pool()
        AnalyticAccountEntry = pool.get('analytic.account.entry')

        if sale.shop:
            for line in sale.lines:
                if line.type != 'line' and line.analytic_accounts:
                    continue
                analytic = sale.shop.analytic_account
                if hasattr(line.product, 'get_analytic') and sale.shop:
                    analytic = line.product.get_analytic(sale.shop, 'income')
                if not analytic:
                    continue
                value = {
                    'root': sale.shop.analytic_account.root,
                    'account': analytic,
                }
                new_entries = AnalyticAccountEntry.create([value])
                line.analytic_accounts = new_entries
                line.save()
        super(Sale, cls).process_sale_pos(sale)
        return sale
